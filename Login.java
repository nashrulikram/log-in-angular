import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-page',
  template: `
    <div>
      <form [formGroup]="loginForm" (ngSubmit)="onSubmit()">
        <input type="text" id="username-input" formControlName="username" (input)="updateUsername($event.target.value)">
        <br>
        <input type="password" id="password-input" formControlName="password" (input)="updatePassword($event.target.value)">
        <br>
        <button type="submit" id="login-button" [disabled]="loginForm.invalid">Submit</button>
      </form>
    </div>
  `
})
export class LoginPageComponent {
  @Output() login: EventEmitter<{username: string, password: string}> = new EventEmitter();

  loginForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  updateUsername(username: string) {
    this.loginForm.patchValue({username: username});
  }

  updatePassword(password: string) {
    this.loginForm.patchValue({password: password});
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const username = this.loginForm.get('username').value;
      const password = this.loginForm.get('password').value;
      this.login.emit({username, password});
    }
  }
}
